unit desktop.loginprofile;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.JSON,
  SmartCL.Storage,
  SmartCL.Storage.Local;

type

  // Exception types
  EWbLoginProfile = class(EW3Exception);


  // This class will save the credentials you have used in the dialog.
  // It will also load the data back, and have a function that checks if
  // the data  exists.
  TWbLoginProfile = class(TObject)
  protected
    // Note: These are class variables and can be changed without any
    // instance. They are placed in protected scope so that only
    // decendants can change them. Remember that these are not bound
    // to an instance, and if you change them - the change is GLOBAL !
    class var __NameSpace: string := 'workbench';
    class var __DataName: string :=  'credentials.';

    // Override these if you need more fields (see published properties)
    procedure ReadFrom(const obj: TJSONObject); virtual;
    procedure WriteTo(const obj: TJSONObject); virtual;
  public
    class function GetResourceLocation(Profilename: string): string; virtual;
    class function QueryAvailableProfiles(var Available: TStrArray): boolean; virtual;

    procedure Clear;
    function  Exists(ProfileName: string): boolean;
    procedure LoadCredentials(ProfileName: string);
    procedure SaveCredentials(ProfileName: string);

    function  &Empty: boolean; virtual;

    property  HostName: string;
    property  Username: string;
    property  Password: string;
  end;


implementation


//#############################################################################
// TWbLoginProfile
//#############################################################################

procedure TWbLoginProfile.Clear;
begin
  HostName := '';
  UserName := '';
  Password := '';
end;

function TWbLoginProfile.&Empty: boolean;
begin
  var LHits := 0;
  if HostName.length < 1 then inc(LHits);
  if UserName.length < 1 then inc(LHits);
  if password.length < 1 then inc(LHits);
  result := LHits = 3;
end;

procedure TWbLoginProfile.ReadFrom(const obj: TJSONObject);
begin
  HostName := obj.ReadString('hostname');
  Username := obj.ReadString('username');
  Password := obj.ReadString('password');
end;

procedure TWbLoginProfile.WriteTo(const obj: TJSONObject);
begin
  obj.AddOrSet('hostname', HostName);
  obj.AddOrSet('username', UserName);
  obj.AddOrSet('password', Password);
end;

class function TWbLoginProfile.GetResourceLocation(Profilename: string): string;
begin
  Profilename := profilename.trim();
  if Profilename.length > 0 then
  begin
    result := __DataName.trim();
    if result.length > 0 then
      result += __DataName;
    result += Profilename;
  end else
  raise EWbLoginProfile.CreateFmt
  ('Method %s failed: Unable to build resource location, profile name is empty error', [{$I %FUNCTION%}]);
end;

class function TWbLoginProfile.QueryAvailableProfiles(var Available: TStrArray): boolean;
begin
  Available.Clear();
  result := false;

  try
    var LStorage := TW3LocalStorage.Create;
    try
      LStorage.Open(__NameSpace);
      try

        var LSignature := __DataName.Trim().ToLower();

        if LocalStorage.Count > 0 then
        begin
          for var x:= 0 to LocalStorage.Count-1 do
          begin
            var LKeyName := LocalStorage.key(x);
            if LKeyName.ToLower().StartsWith(LSignature) then
              Available.add( LKeyName );
          end;
        end;

        result := Available.Count > 0;

      finally
        if LStorage.Active then
          LStorage.Close();
      end;
    finally
      LStorage.free;
    end;
  except
    on e: exception do;
  end;
end;

function TWbLoginProfile.Exists(ProfileName: string): boolean;
var
  LStorage: TW3LocalStorage;
begin
  ProfileName := ProfileName.trim();
  if ProfileName.length > 0 then
  begin
    try
      LStorage := TW3LocalStorage.Create;
      try
        LStorage.Open(__Namespace);
        try

          var ResLocation := GetResourceLocation(Profilename);
          result := LStorage.GetKeyExists( ResLocation );

        finally
          if LStorage.Active then
            LStorage.Close();
        end;
      finally
        LStorage.free;
      end;
    except
      on e: exception do
      raise EWbLoginProfile.CreateFmt
      ('Method %s failed: System threw exception %s with message: %s', [{$I %FUNCTION%}, e.classname, e.message]);
    end;
  end else
  raise EWbLoginProfile.CreateFmt('Method %s failed: Profile name is empty error', [{$I %FUNCTION%}]);
end;

procedure TWbLoginProfile.LoadCredentials(ProfileName: string);
var
  LStorage: TW3LocalStorage;
  ResLocation: string;
begin
  Clear();

  // Make sure profilename is not empty
  ProfileName := ProfileName.trim();
  if ProfileName.length > 0 then
  begin
    // build the RL or "filename" for our namespace in the cache
    ResLocation := GetResourceLocation(Profilename);

    // Make sure there is data at the resource-location
    if Exists(Profilename) then
    begin
      try

        LStorage := TW3LocalStorage.Create;
        try
          LStorage.Open(__Namespace);

          try

            var LData := LStorage.GetKeyStr(ResLocation, '');

            if LData.length > 0 then
            begin
              var obj := TJSONObject.Create;
              try
                obj.FromJSON(LData);
                ReadFrom(obj);
              finally
                obj.free;
              end;
            end else
            raise EWbLoginProfile.CreateFmt
            ('Failed to load credentials profile (%s), file contains no data error', [ProFilename]);

          finally
            if LStorage.Active then
              LStorage.Close();
          end;
        finally
          LStorage.free;
        end;

      except
        on e: exception do
        raise EWbLoginProfile.CreateFmt
        ('Method %s failed: System threw exception %s with message: %s', [{$I %FUNCTION%}, e.classname, e.message]);
      end;

    end else
    raise EWbLoginProfile.CreateFmt
    ('Method %s failed: Profile (%s) resource not found [%s] error', [{$I %FUNCTION%}, ProFilename, ResLocation]);

  end else
  raise EWbLoginProfile.CreateFmt('Method %s failed: Profile name is empty error', [{$I %FUNCTION%}]);
end;

procedure TWbLoginProfile.SaveCredentials(ProfileName: string);
var
  LStorage: TW3LocalStorage;
begin
  ProfileName := ProfileName.trim();
  if ProfileName.length > 0 then
  begin
    try

      LStorage := TW3LocalStorage.Create;
      try
        LStorage.Open(__Namespace);
        try

          // Create JSON envelope
          var obj := TJSONObject.Create;
          try
            // write credentials-info to JSON object
            WriteTo(obj);

            // Build Resource Location [keyname]
            var ResLocation := GetResourceLocation(Profilename);

            // Write data as JSON
            LStorage.SetKeyStr(ResLocation, obj.ToJSON() );

          finally
            obj.free;
          end;

        finally
          if LStorage.Active then
            LStorage.Close();
        end;
      finally
        LStorage.free;
      end;

    except
      on e: exception do
      raise EWbLoginProfile.CreateFmt
      ('Failed to save profile, system threw exception %s with message: %s', [e.classname, e.message]);
    end;
  end else
  raise EWbLoginProfile.Create('Failed to save profile, name was empty error');
end;


end.
