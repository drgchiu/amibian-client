unit desktop.toolbar;

{$I 'Smart.inc'}

interface

uses
  W3C.DOM,
  System.Types,
  SmartCL.Theme,

  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,

  desktop.control,

  SmartCL.System,
  SmartCL.Components,
  SmartCL.Controls.Image;

type
  TWbToolbarElement = class;
  TWbToolbarSeparator = class;
  TWbToolbarButton = class;
  TWbToolbar = class;

  TWbToolbarElementAlignment = (
    elAlignLeft,
    elAlignRight
  );

  TWbToolbarJustification = (
    tjPacked,
    tjSpaceEvenly,
    tjSpaceAround
  );

  TWbToolbarElement = class(TWbCustomControl)
  private
    function  GetIndex: integer;
  protected
    procedure SetWidth(const NewWidth: integer); override;
    procedure SetSize(const NewWidth, NewHeight: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); override;

    procedure CBClick(EventObj: JEvent); override;

    function  AcceptOwner(const CandidateObject: TObject): boolean; override;
    function  GetToolbar: TWbToolbar; virtual;
    procedure InitializeObject; override;
  public
    property  Index: integer read GetIndex;
    property  Toolbar: TWbToolbar read GetToolbar;
  end;

  TWbToolbarSeparator = class(TWbToolbarElement)
  end;

  TWbToolbarButton = class(TWbToolbarElement)
  private
    FGlyph:     TW3Image;
    FCaption:   string;
    FDown:      boolean;
    FLayout:    TW3GlyphLayout = glGlyphTop;
    procedure   HandleGlyphReady(Sender: TObject);
  protected
    procedure   SetDownState(NewDownState: boolean);
    procedure   InternalSetCaption(const NewCaption: string); virtual;
    procedure   SetCaption(NewCaption: string); virtual;
    procedure   SetLayout(const NewLayout: TW3GlyphLayout); virtual;

    procedure   CBClick(EventObj: JEvent); override;

    procedure   InitializeObject; override;
    procedure   FinalizeObject; override;
    function    MakeElementTagObj: THandle; override;

  published
    property    Layout: TW3GlyphLayout read FLayout write SetLayout;
    property    Glyph: TW3Image read FGlyph;
    property    Caption: string read FCaption write SetCaption;
    property    AllowAllUp: boolean;
    property    Down: boolean read FDown write SetDownState;
    property    GroupIndex: integer;
  end;

  TWbToolbarElementClickedEvent = procedure (Sender: TObject);
  TWbToolbarDownChangeEvent     = procedure (Sender: TObject; Previous, Current: TWbToolbarElement);

  TWBToolbarLayout = (
    tlHorizontal,
    tlVertical
  );

  TWbToolbar = class(TWbCustomControl)
  private
    FButtonWidth:   integer  = 100;
    FButtonHeight:  integer  = 30;
    FSeperatorSize: integer  = 16;
    FLayout:        TWBToolbarLayout = tlHorizontal;
    FJustify:       TWbToolbarJustification = tjPacked;
    FAlignment:     TWbToolbarElementAlignment = elAlignLeft;
    FGCollection:   array of TWbToolbarElement;
    FOnClicked:     TWbToolbarElementClickedEvent;
  protected
    procedure   SetLayout(const NewLayout: TWBToolbarLayout); virtual;
    procedure   SetJustify(const NewJustify: TWbToolbarJustification); virtual;
    procedure   SetAlignment(const NewAlignment: TWbToolbarElementAlignment); virtual;
    procedure   SetSeperatorSize(const NewSeperatorSize: integer); virtual;
    procedure   SetButtonWidth(const NewWidth: integer); virtual;
    procedure   SetButtonHeight(const NewHeight: integer); virtual;

    procedure   InitializeObject; override;
    procedure   Resize; override;

    procedure   ChildElementClicked(const Child: TWbToolbarElement); virtual;
    procedure   ChildElementDown(const Child: TW3CustomControl); virtual;

    procedure   ChildAdded(const NewChild: TW3TagContainer); override;
    procedure   ChildRemoved(const OldChild: TW3TagContainer); override;
  public
    property    Count: integer read (GetChildCount);
    property    Items[const Index: integer]: TWbToolbarElement
                read  ( TWbToolbarElement(GetChildObject(Index)) ); default;

    function    GetSelectedInGroup(const Group: integer): TWbToolbarButton;
    procedure   ClearGroupSelected(const GroupIndex: integer);

    function    CreationFlags: TW3CreationFlags; override;
    procedure   Remove(const Element: TWbToolbarElement);
    function    Add: TWbToolbarButton;
    function    AddSeparator: TWbToolbarSeparator;
  published
    property    Layout: TWBToolbarLayout read FLayout write SetLayout;
    property    ElementAlignment: TWbToolbarElementAlignment read FAlignment write SetAlignment;
    property    ElementJustify: TWbToolbarJustification read FJustify write SetJustify;
    property    SeparatorSize: integer read FSeperatorSize write SetSeperatorSize;
    property    ButtonWidth: integer read FButtonWidth write SetButtonWidth;
    property    ButtonHeight: integer read FButtonHeight write SetButtonHeight;


    property    OnElementClicked: TWbToolbarElementClickedEvent read FOnClicked write FOnClicked;

  end;

implementation

//#############################################################################
// TWbToolbarElement
//#############################################################################

procedure TWbToolbarElement.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode :=  dmBlock;
end;

function TWbToolbarElement.GetIndex: integer;
begin
  var tb := GetToolbar();
  if tb <> nil then
    result := tb.IndexOfChild(self)
  else
    result := -1;
end;

function TWbToolbarElement.AcceptOwner(const CandidateObject: TObject): boolean;
begin
  // Toolbar buttons can only be created on toolbars
  result := (CandidateObject <> nil) and (CandidateObject is TWbToolbar);
end;

function TWbToolbarElement.GetToolbar: TWbToolbar;
begin
  if Parent <> nil then
    result := Parent as TWbToolbar;
end;

procedure TWbToolbarElement.CBClick(EventObj: JEvent);
begin
  inherited CBClick(EventObj);

  // Signal toolbar of click
  var Temp := GetToolbar();
  if Temp <> nil then
    Temp.ChildElementClicked(self);
end;

procedure TWbToolbarElement.SetWidth(const NewWidth: integer);
begin
  inherited SetWidth(NewWidth);
  Handle.style['flex-basis'] := Tinteger.ToPxStr(NewWidth);
end;

procedure TWbToolbarElement.SetSize(const NewWidth, NewHeight: integer);
begin
  inherited SetSize(NewWidth, NewHeight);
  Handle.style['flex-basis'] := Tinteger.ToPxStr(NewWidth);
end;

procedure TWbToolbarElement.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
  inherited SetBounds(NewLeft, NewTop, NewWidth, NewHeight);
  Handle.style['flex-basis'] := Tinteger.ToPxStr(NewWidth);
end;

//#############################################################################
// TWbToolbarButton
//#############################################################################

procedure TWbToolbarButton.InitializeObject;
begin
  inherited;
  FGlyph := TW3Image.Create(nil);
  FGlyph.OnLoad := HandleGlyphReady;
  FDown := false;
end;

procedure TWbToolbarButton.FinalizeObject;
begin
  FGlyph.OnLoad := nil;
  FGlyph.Free;
  inherited;
end;

procedure TWbToolbarButton.CBClick(EventObj: JEvent);
begin
  inherited CBClick(EventObj);

  if GroupIndex > 0 then
  begin
    if not FDown then
      SetDownState( not FDown)
    else
      exit;
  end;
end;

procedure TWbToolbarButton.SetDownState(NewDownState: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewDownState <> FDown then
    begin
      var tb := GetToolbar();

      if GroupIndex > 0 then
      begin
        if not Down then
        begin
          if tb <> nil then
            tb.ClearGroupSelected(GroupIndex);
        end else
        if AllowAllUp then
          FDown := false;
      end;

      case FDown of
      false:
        begin
          ThemeBackground := TW3ThemeBackground.bsDecorativeBackground;
          ThemeBorder := TW3ThemeBorder.btDecorativeBorder;
          FDown := true;
        end;
      true:
        begin
          ThemeBackground := TW3ThemeBackground.bsToolButtonBackground;
          ThemeBorder := TW3ThemeBorder.btToolButtonBorder;
          FDown := false;
        end;
      end;

      // Signal toolbar of change
      if tb <> nil then
        tb.ChildElementDown(self);
    end;
  end;
end;

function TWbToolbarButton.MakeElementTagObj: THandle;
begin
  result := w3_createHtmlElement('button');
end;

procedure TWbToolbarButton.SetLayout(const NewLayout: TW3GlyphLayout);
begin
  if NewLayout <> FLayout then
  begin
    FLayout := NewLayout;
    if (csReady in ComponentState) then
      InternalSetCaption(FCaption);
  end;
end;

procedure TWbToolbarButton.SetCaption(NewCaption: string);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewCaption := NewCaption.trim();
    if (NewCaption <> FCaption) then
    begin
      FCaption := NewCaption;
      InternalSetCaption(NewCaption)
    end;
  end;
end;

procedure TWbToolbarButton.InternalSetCaption(const NewCaption: string);

  function MakeImgTag(src: string; w, h: integer): string;
  begin
    result := '<img dragable=false src="' + src + '" '
              + 'width='  + TInteger.ToPxStr(w) + ' '
              + 'height=' + TInteger.ToPxStr(h) + '>';
  end;

begin
  var Html := '';
  if FGlyph.Ready then
  begin
    var wd := FGlyph.PixelWidth;
    var hd := FGlyph.PixelHeight;

    Html := '<table border="0">';
    case FLayout of
    glGlyphTop:
      begin
        Html += '<tr>';
        Html += '<td>';
        Html += MakeImgTag(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';

        Html += '<tr>';
        Html += '<td><center>' + NewCaption + '</td>';
        Html += '</tr>';
      end;
    glGlyphLeft:
      begin
        Html += '<tr>';
        Html += '<td>';
        Html += MakeImgTag(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '<td>';
        Html += '<center>' + NewCaption;
        Html += '</td>';
        Html += '</tr>';
      end;
    glGlyphRight:
      begin
        Html += '<tr>';
        Html += '<td>';
        Html += '<center>' + NewCaption;
        Html += '</td>';
        Html += '<td>';
        Html += MakeImgTag(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';
      end;
    glGlyphBottom:
      begin
        Html += '<tr>';
        Html += '<td><center>' + NewCaption + '</td>';
        Html += '</tr>';
        Html += '<tr>';
        Html += '<td>';
        Html += MakeImgTag(FGlyph.Url, wd, hd);
        Html += '</td>';
        Html += '</tr>';
      end;
    end;
    Html += '</table>';
  end else
  Html := NewCaption;

  InnerHTML := Html;
end;

procedure TWbToolbarButton.HandleGlyphReady(Sender: TObject);
begin
  InternalSetCaption(FCaption);
end;

//#############################################################################
// TWbToolbar
//#############################################################################

procedure TWbToolbar.InitializeObject;
begin
  inherited;
  Displaymode := dmFlex;
  SimulateMouseEvents := true;
  TransparentEvents := true;
end;

function TWbToolbar.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Include(result, cfSupportAdjustment);
  Include(result, cfReportChildAddition);
  Include(result, cfReportChildRemoval);
  Include(result, cfReportResize);
  Exclude(result, cfReportMovement);
end;

procedure TWbToolbar.ClearGroupSelected(const GroupIndex: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate();
    try
      for var i := 0 to Count - 1 do
      begin
        if (Items[i] is TWbToolbarButton) then
        begin
          var LButton := TWbToolbarButton(Items[i]);
          if (LButton.GroupIndex = GroupIndex) then
          begin
            if LButton.Down then
              LButton.Down := false;
          end;
        end;
      end;
    finally
      EndUpdate();
    end;
  end;
end;

procedure TWbToolbar.ChildElementDown(const Child: TW3CustomControl);
begin
end;

procedure TWbToolbar.ChildElementClicked(const Child: TWbToolbarElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    if Child <> nil then
    begin
      if assigned(OnElementClicked) then
        OnElementClicked(Child);
    end;
  end;
end;

procedure TWbToolbar.SetJustify(const NewJustify: TWbToolbarJustification);
var
  LData:  string;
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewJustify <> FJustify then
    begin
      BeginUpdate();
      try
        FJustify := NewJustify;
        case NewJustify of
        tjPacked:       LData := 'flex-start';
        tjSpaceEvenly:  LData := 'space-evenly';
        tjSpaceAround:  LData := 'space-around';
        end;
        Handle.style['justify-content'] := LData;
      finally
        EndUpdate();
      end;
    end;
  end;
end;

procedure TWbToolbar.SetLayout(const NewLayout: TWBToolbarLayout);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewLayout <> FLayout then
    begin
      BeginUpdate();
      FLayout := NewLayout;

      case NewLayout of
      tlHorizontal: Handle.style['flex-direction'] := 'row';
      tlVertical:   Handle.style['flex-direction'] := 'column';
      end;

      EndUpdate();
    end;
  end;
end;

procedure TWbToolbar.SetAlignment(const NewAlignment: TWbToolbarElementAlignment);
begin
  if not (csDestroying in ComponentState) then
  begin
    if NewAlignment <> FAlignment then
    begin
      BeginUpdate;
      FAlignment := NewAlignment;

      for var x := 0 to Count-1 do
      begin
        var Item = Items[x];
        if Item is TWbToolbarButton then
        begin

          case NewAlignment of
          elAlignLeft:
            begin
              Item.TagStyle.RemoveByName("TWbToolElementAlignRight");
              Item.TagStyle.Add("TWbToolElementAlignLeft");
            end;
          elAlignRight:
            begin
              Item.TagStyle.RemoveByName("TWbToolElementAlignLeft");
              Item.TagStyle.Add("TWbToolElementAlignRight");
            end;
          end;

        end;
      end;

      AddToComponentState([csSized]);
      EndUpdate;
    end;

  end;
end;

procedure TWbToolbar.SetSeperatorSize(const NewSeperatorSize: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := if NewSeperatorSize >16 then NewSeperatorSize else 16;
    if Temp <> FSeperatorSize then
    begin
      BeginUpdate;
      FSeperatorSize := Temp;
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

procedure TWbToolbar.SetButtonWidth(const NewWidth: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := if NewWidth >16 then NewWidth else 16;
    if Temp <> FButtonWidth then
    begin
      BeginUpdate;
      FButtonWidth := Temp;
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

procedure TWbToolbar.SetButtonHeight(const NewHeight: integer);
var
  Temp: integer;
begin
  if not (csDestroying in ComponentState) then
  begin
    Temp := if NewHeight >16 then NewHeight else 16;
    if Temp <> FButtonHeight then
    begin
      BeginUpdate;
      FButtonHeight := Temp;
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

procedure TWbToolbar.Resize;
begin
  inherited;

  for var x := 0 to Count-1 do
  begin
    var LItem := Items[x];

    if (LItem is TWbToolbarSeparator) then
    begin
      if LItem.Width <> FSeperatorSize then
        LItem.width := FSeperatorSize;

      if w3_GetStyleAsStr(LItem.Handle,'height') <> "100%" then
        LItem.Handle.style.height := '100%';
    end else
    if (LItem is TWbToolbarElement) then
    begin
      if (LItem.Height <> FButtonHeight) then
        LItem.SetSize(FButtonWidth, FButtonHeight);
    end;
  end;
end;

function TWbToolbar.Add: TWbToolbarButton;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate;
    try
      result := TWbToolbarButton.Create(nil);
      result.Width := FButtonWidth;
      result.Height := FButtonHeight;

      case FAlignment of
      elAlignLeft:  result.TagStyle.Add("TWbToolElementAlignLeft");
      elAlignRight: result.TagStyle.Add("TWbToolElementAlignRight");
      end;

      // Add item to internal "im working on it" list
      FGCollection.Add(result);

      // And attach the element to this control
      RegisterChild(result);

    finally
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

function TWbToolbar.AddSeparator(): TWbToolbarSeparator;
begin
  if not (csDestroying in ComponentState) then
  begin
    BeginUpdate;
    try
      result := TWbToolbarSeparator.Create(nil);
      result.width := FSeperatorSize;
      result.Handle.style.height := '100%';

      case FAlignment of
      elAlignLeft:  result.TagStyle.Add("TWbToolElementAlignLeft");
      elAlignRight: result.TagStyle.Add("TWbToolElementAlignRight");
      end;

      // Add item to internal "im working on it" list
      FGCollection.Add(result);

      // And attach the element to this control
      RegisterChild(result);

    finally
      AddToComponentState([csSized]);
      EndUpdate;
    end;
  end;
end;

function TWbToolbar.GetSelectedInGroup(const Group: integer): TWbToolbarButton;
begin
  if not (csDestroying in ComponentState) then
  begin
    for var x :=0 to GetChildCount-1 do
    begin
      var obj := GetChildObject(x);
      if (obj is TWbToolbarButton) then
      begin
        var btn := TWbToolbarButton(obj);
        if btn.GroupIndex = Group then
        begin
          if btn.Down then
          begin
            result := btn;
            break;
          end;
        end;
      end;
    end;
  end;
end;

procedure TWbToolbar.ChildAdded(const NewChild: TW3TagContainer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (NewChild is TWbToolbarElement) then
    begin
      // Check if we issued the add
      var GCIndex := FGCollection.IndexOf(TWbToolbarElement(NewChild));

      if GCIndex >= 0 then
      begin
        // Yes we did, it's all good
        FGCollection.Delete(GCIndex, 1)
      end else
      begin
        // The user just created one directly, so we
        // Issue a call to invalidate() to clean things up
        if (csReady in ComponentState) then
          Invalidate();
      end;
    end;
  end;

  inherited ChildAdded(NewChild);
end;

procedure TWbToolbar.ChildRemoved(const OldChild: TW3TagContainer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (OldChild is TWbToolbarElement) then
    begin
      // Check if we issued the free call
      var GCIndex := FGCollection.IndexOf(TWbToolbarElement(OldChild));

      if GCIndex >= 0 then
      begin
        // Yes we did, it's all good
        FGCollection.Delete(GCIndex, 1)
      end else
      begin
        // The user just killed an element directly
        // Issue a call to invalidate() to clean things up
        if (csReady in ComponentState) then
          Invalidate();
      end;
    end;
  end;

  inherited ChildRemoved(OldChild);
end;

procedure TWbToolbar.Remove(const Element: TWbToolbarElement);
begin
  if not (csDestroying in ComponentState) then
  begin
    var Temp := IndexOfChild(Element);
    if Temp >= 0 then
    begin
      // Add item to internal "im working on it" list
      var ToolItem := Items[Temp];
      FGCollection.Add(ToolItem);

      BeginUpdate;
      try
        // Sink any exception at this point
        try
          ToolItem.free;
        except
          on e: exception do;
        end;
      finally
        EndUpdate;
      end;
    end;
  end;
end;


initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := StrReplace(
  #"

  .TWbToolbar {
    background: #EFEFEF;
    border-style: solid;
    border-left-width: 1px;
    border-left-color: #FFFFFF;
    border-top-width: 1px;
    border-top-color: #FFFFFF;
    border-bottom-width: 1px;
    border-bottom-color: #93939A;
    border-right-width: 1px;
    border-right-color: #93939A;
    flex-flow: row;
    flex-wrap: nowrap;
    align-items: center;
  }

  .TWbToolbar :first-child {
    margin-left: 2px !important;
  }

  .TWbToolbar > * {
    margin-right: 4px !important;
  }

  .TWbToolElementAlignLeft {
    float: left !important;
  }

  .TWbToolElementAlignRight {
    float: right !important;
  }

  .TWbToolbarButton {
    border-top: 1px solid #E0E0E0;
    border-left: 1px solid #E0E0E0;
    border-bottom: 1px solid #979797;
    border-right: 1px solid #979797;

    font-family: Ubuntu, tahoma, verdana;

    outline: 1px solid #42415A;
    outline-offset: 0px;

    background-image: -ms-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -moz-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -o-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
    background-image: -webkit-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: linear-gradient(to bottom, #B0B0B0 0%, #D7D7D7 100%);

    font-size: normal;
    text-align: center;
    font-weight: normal;
    §text-size-adjust: auto;
  }

  .TWbToolbarButton:active {
    border-top: 1px solid #979797;
    border-left: 1px solid #979797;
    border-bottom: 1px solid #E0E0E0;
    border-right: 1px solid #E0E0E0;

    background-image: -ms-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -moz-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -o-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -webkit-gradient(linear, left bottom, right top, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
    background-image: -webkit-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: linear-gradient(to top right, #B0B0B0 0%, #D7D7D7 100%);
  }

  ","§",prefix);
  Sheet.Append(StyleCode);
end;

end.
