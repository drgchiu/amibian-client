unit desktop.control.pathpanel;

interface

uses
  W3C.DOM,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,
  System.Widget,
  System.IOUtils,

  desktop.types,
  desktop.window,
  desktop.iconView,
  desktop.panel,
  desktop.edit,
  desktop.button,
  desktop.toolbar,
  desktop.scrollbar,
  desktop.icons,

  SmartCL.Time,
  SmartCL.System,
  SmartCL.Layout,
  SmartCL.Controls.Elements,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Effects,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,

  System.Device.Storage,
  Desktop.FileSystem.Node,

  SmartCL.Controls.Image,
  SmartCL.Controls.Label;

type

  TWbPathPanel = class(TWbPanel)
  private
    FSelect:  TWbButton;
    FEdit:    TWbEditbox;
    FLayout:  TLayout;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  SelectFolder: TWbButton read FSelect;
    property  Edit: TWbEditBox read FEdit;
  end;


implementation


//#############################################################################
// TWbPathPanel
//#############################################################################

procedure TWbPathPanel.InitializeObject;
begin
  inherited;
  FSelect := TWbButton.Create(self);
  FSelect.SetSize(130 ,28);

  FEdit := TWbEditbox.Create(self);
  FEdit.SetSize(200, 26);
end;

procedure TWbPathPanel.FinalizeObject;
begin
  FEdit.free;
  FSelect.free;
  inherited;
end;

procedure TWbPathPanel.ObjectReady;
begin
  inherited;
  TW3Dispatch.WaitFor([FSelect, FEdit], procedure ()
  begin
    FSelect.Enabled := false;
    FEdit.ReadOnly := true;
    FSelect.Caption := 'Set Destination';

    FLayout := Layout.client(Layout.Margins(4).Spacing(4),
      [
        Layout.left(FSelect),
        Layout.Client(Layout.Stretch, FEdit)
      ]
      );

    Resize();
  end);
end;

procedure TWbPathPanel.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    if TW3Dispatch.AssignedAndReady([FEdit, FSelect]) then
    begin
      if FLayout <> nil then
        FLayout.Resize(self);
    end;
  end;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := StrReplace( //EFECE9
  #"
  .TWbPathPanel {
    padding: 0px !important;
    margin: 0px !important;
  }

  ","§",prefix);
  Sheet.Append(StyleCode);
end;


end.
