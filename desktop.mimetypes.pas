unit desktop.mimetypes;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.IOUtils
  {System.Device.Storage,
  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System};

type


  TGraphicMimeTypes = class static
  public
    class function  GetMimeTypeFromFilename(Filename: string): string;
    class function  GetMimeTypeFromStream(const Stream: TStream): string;
    class function  StreamContainsJPG(const Stream: TStream): boolean;
    class function  StreamContainsGIF(const Stream: TStream): boolean;
    class function  StreamContainsPNG(const Stream: TStream): boolean;
    class function  StreamContainsICO(const Stream: TStream): boolean;
    class function  StreamContainsWebP(const Stream: TStream): boolean;
    class function  StreamContainsTIFF(const Stream: TStream): boolean;
    class function  StreamContainsKnownGraphic(const Stream: TStream): boolean;
  end;


implementation

class function TGraphicMimeTypes.GetMimeTypeFromFilename(Filename: string): string;
begin
  filename := Filename.Trim().ToLower();
  if filename.length > 0 then
  begin
    var LExt := TPath.GetExtension(Filename);
    case LExt of
    '.jpg',
    '.jpeg':  result := 'image/jpeg';
    '.png':   result := 'image/png';
    '.gif':   result := 'image/gif';
    '.ico':   result := 'image/x-icon';
    '.svg':   result := 'image/svg+xml';
    '.tif':   result := 'image/tiff';
    '.tiff':  result := 'image/tiff';
    '.webp':  result := 'image/webp';
    else
      result := 'application/octet-stream';
    end;
  end;
end;

class function TGraphicMimeTypes.GetMimeTypeFromStream(const Stream: TStream): string;
begin
  if Stream <> nil then
  begin
    if StreamContainsJPG(Stream) then result := 'image/jpeg' else
    if StreamContainsGIF(Stream) then result := 'image/gif' else
    if StreamContainsPNG(Stream) then result := 'image/png' else
    if StreamContainsICO(Stream) then result := 'image/x-icon' else
    if StreamContainsWebP(Stream) then result := 'image/webp' else
    if StreamContainsTIFF(Stream) then result := 'image/tiff';
  end else
    result := 'application/octet-stream';
end;

class function TGraphicMimeTypes.StreamContainsKnownGraphic(const Stream: TStream): boolean;
begin
  if Stream <> nil then
    result := StreamContainsJPG(Stream)
      or StreamContainsGIF(Stream)
      or StreamContainsPNG(Stream)
      or StreamContainsICO(Stream)
      or StreamContainsWebP(Stream)
      or StreamContainsTIFF(Stream);
end;

class function TGraphicMimeTypes.StreamContainsPNG(const Stream: TStream): boolean;
begin
  // Note: Endian is already handled by TStream's underlying buffer
  if Stream <> nil then
  begin
    if Stream.size > 4 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(8);
      Stream.Position := LOldPos;

      if LSign.length = 8 then
        result := (LSign[0] = 137)
              and (LSign[1] = 80)
              and (LSign[2] = 78)
              and (LSign[3] = 71)
              and (LSign[4] = 13)
              and (LSign[5] = 10)
              and (LSign[6] = 26)
              and (LSign[7] = 10);
    end;
  end;
end;

class function TGraphicMimeTypes.StreamContainsWebP(const Stream: TStream): boolean;
begin
  if Stream <> nil then
  begin
    if Stream.size > 4 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(4);
      Stream.Position := LOldPos;

      if LSign.Length = 4 then
        result := (LSign[0] = $52)
              and (LSign[1] = $49)
              and (LSign[2] = $46)
              and (LSign[3] = $46);
    end;
  end;
end;

class function TGraphicMimeTypes.StreamContainsICO(const Stream: TStream): boolean;
begin
  if Stream <> nil then
  begin
    if Stream.size > 4 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(4);
      Stream.Position := LOldPos;

      if LSign.Length = 4 then
        result := (LSign[0] = $00)
              and (LSign[1] = $00)
              and (LSign[2] = $01)
              and (LSign[3] = $00);
    end;
  end;
end;

class function TGraphicMimeTypes.StreamContainsGIF(const Stream: TStream): boolean;
begin
  if Stream <> nil then
  begin
    if Stream.size > 4 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(4);
      Stream.Position := LOldPos;

      if LSign.Length = 4 then
        result := (LSign[0] = $47)
              and (LSign[1] = $49)
              and (LSign[2] = $46)
              and (LSign[3] = $38);
    end;
  end;
end;

class function TGraphicMimeTypes.StreamContainsJPG(const Stream: TStream): boolean;
begin
  if Stream <> nil then
  begin
    if Stream.size > 3 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(3);
      Stream.Position := LOldPos;

      if LSign.length = 3 then
        result := (LSign[0] = $FF)
              and (LSign[1] = $D8)
              and (LSign[2] = $FF);
    end;
  end;
end;

class function TGraphicMimeTypes.StreamContainsTIFF(const Stream: TStream): boolean;
begin
  if Stream <> nil then
  begin
    if Stream.size > 3 then
    begin
      var LOldPos := Stream.Position;
      var LSign := Stream.read(3);
      Stream.Position := LOldPos;

      if LSign.length = 3 then
        result := (LSign[0] = $49)
              and (LSign[1] = $20)
              and (LSign[2] = $49);
    end;
  end;
end;



end.
