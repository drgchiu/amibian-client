unit desktop.window.welcome;

interface

uses
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Time,
  System.Colors,

  Desktop.Types,
  Desktop.Control,
  Desktop.Window,

  SmartCL.Effects,
  SmartCL.Scroll,
  SmartCL.Time,
  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,
  SmartCL.Components,
  SmartCL.Controls.Label,
  SmartCL.Controls.ScrollBox,
  desktop.scrollbar,
  SmartCL.System;

type

  TWbWelcomeScrollBox = class(TW3ScrollBox)
  end;

  TWbWelcomeContent = class(TWbWindowContent)
  private
    FBox:     TWbWelcomeScrollBox;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
  public
    property  Scrollbox: TW3ScrollBox read FBox;
  end;

  TWbWelcomeWindow = class(TWbWindow)
  protected
    procedure InitializeObject; override;
    procedure ObjectReady; override;

    procedure UpdateScrollValues;
    procedure WindowScrollbarScrolled(Sender: TObject);
    procedure WindowContentScrolled(Sender: TObject; dx, dy: integer);

    procedure Resize; override;
    procedure AfterResize; override;

    function  CreateContentInstance: TWbWindowContent; override;
  public
    property  Content: TWbWelcomeContent read ( TWbWelcomeContent(inherited Content) );
    property  ScrollContent: TW3ScrollContent read ( Content.ScrollBox.Content );
  end;


implementation

//#############################################################################
// TWbWelcomeWindow
//#############################################################################

procedure TWbWelcomeWindow.InitializeObject;
begin
  inherited;
  TransparentEvents := true;
  Options := [woSizeable, woVScroll];

  RightEdge.Scrollbar.Position := 0;
end;

function TWbWelcomeWindow.CreateContentInstance: TWbWindowContent;
begin
  result := TWbWelcomeContent.Create(self);
end;

procedure TWbWelcomeWindow.WindowContentScrolled(Sender: TObject; dx, dy: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    Content.Scrollbox.ScrollController.Enabled := false;
    RightEdge.Scrollbar.Position := dy;
    Content.Scrollbox.ScrollController.Enabled := true;
  end;
end;

procedure TWbWelcomeWindow.WindowScrollbarScrolled(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    var LScrollbar :=  TWbVerticalScrollbar(sender);
    var LPos := LScrollbar.Position;
    Content.Scrollbox.ScrollController.ScrollTo(0, -LPos);
  end;
end;

procedure TWbWelcomeWindow.ObjectReady;
begin
  inherited;

  Content.Scrollbox.background.FromColor(CNT_STYLE_Background);
  Content.Scrollbox.Content.Height := 0;
  Content.Scrollbox.Content.InnerHtml :=
        #'
        <div class = "DocHeader">Smart Mobile Studio 3.0</div>
        <p style="margin-left: 4px">Copyright © 2009 -2018 The Smart Company AS<br>
        All rights reserved</p>
        <hr>

        <p class="TopicHeader">Introduction to Smart Mobile Studio</p>

        <p class="DocText">Smart Mobile Studio is a software development platform that
        consists of a compiler, run-time library and integrated
        development environment for writing modern, web based software.</p>

        <p class="DocText">Our compiler reads a dialect of <a href="https://en.wikipedia.org/wiki/Smart_Pascal">object pascal</a>,
        a language that is easy to learn, powerful (comparable to C# and C++),
        and extremely productive; but instead of compiling to
        traditional machine code, our compiler emits cutting edge
        JavaScript. JavaScript that runs both in the browser and on the server.</p>

        <p class="DocText">As the name implies Smart is a brilliant system for developing
        mobile applications. Our RTL has plenty of visual controls (and non-visual classes)
        that covers almost every aspect of what the browser has to offer.
        The RTL is written from scratch in Smart itself, so you can view the
        code directly, inherit from the classes, create your own custom
        controls and work in the same manner as you would in Visual Studio,
        Delphi or Lazarus.</p>

        <p class="DocText">As you no doubt have noticed, Smart is not limited to <i>just</i>
        mobile applications. You can write powerful web applications
        like the one you are looking at right now. Both the desktop and the
        server supporting it were written 100% using Smart Mobile Studio.</p>

        <p class="DocText">If you would like to learn more about Smart Mobile Studio, please
        visit <a href="http://www.smartmobilestudio.com/">
        our website</a>. We are sure you will be amazed and impressed by how
        powerful and productive our dialect of object-pascal is for web
        development. There is nothing quite like Smart.</p>

        <p class="TopicHeader">Web desktop demo</p>

        <p class="DocText">The system you are looking at right now is a demo of what you
        can build using Smart Mobile Studio. This is <i>not</i> a mock-up desktop,
        but a framework for writing large and complex applications that should
        run directly in the browser. With large applications we are talking Adobe
        Photoshop style web applications. Applications that would take ages
        to write in raw, vanilla JavaScript.</p>

        <p class="DocText">For example, when you double-click a drive or folder icon, you are
        exploring <i>the actual</i> files managed by the server. The Smart RTL have
        established standards for almost everything, file management included;
        so writing a new "filesystem driver [class]" to wrap existing web storage
        providers is reduced to childs play. In our case we have a filesystem
        class that talks directly with the server via the Ragnarok message API.

        <p class="TopicHeader">The Ragnarok message server</p>

        <p class="DocText">
        Smart Mobile Studio 3.0 has awesome support for node.js; far better
        than any other compiler targeting JavaScript. We have server classes that
        wrap and gives you easy access to nodejs many modules, which as of
        writing includes: </p>

        <ul>
        <li>http and https</li>
        <li>websocket and websocket-secure</li>
        <li>udp [excellent for online games]</li>
        <li>raw tcp server</li>
        <li>pipes [good for inter-process communication]</li>
        </ul>
        <p class="DocText">
        People find that implementing a custom server in Smart
        Pascal takes much less time than Delphi or C#, which is a really
        cool thing.
        </p>

        <p class="DocText">The Ragnarok framework is both a server and a message
        stack. A message stack is a way of registering messages that the
        server should support, each message representing a call (remote
        procedure call), which is bound to spesific request and response objects.</p>

        <p class="DocText">The Ragnarok server is based on websocket, which is
        a great protocol because its low-latency, long-term and full duplex.
        Full duplex means both the client and the server can send messages
        at the same time. So it breaks free from the traditional request and
        response behavior of HTTP. So its brilliant for our web desktop
        framework, chat services - even video chat would benefit from using
        websocket</p>

        <p class="DocText"><u>Note:</u> It is important to understand that there are no
        hardcoded limitations in our RTL. The RTL is written using Smart Mobile Studio
        itself. The various class hierarchies and namespaces can all be viewed,
        altered, inherited from and shaped as you see fit.
        So if Ragnarok doesnt fit the bill, or you fancy a more compact
        message format (our message objects serialize to JSON),
        then you are free to implement your own system.</p>

        <p class="TopicHeader">Awesome demos</p>

        <p class="DocText">On the right side of the desktop there is a toolbar
        with various icons on it. These are external applications (which under
        web means they are external web sites) and demonstrations that you can
        run in a window. There are some really awesome demos lined up, showing
        some of the potential emerging cloud and web technology brings</p>

        <p class="TopicHeader">The Quake 3 demo</p>

        <p class="DocText">Besides the impressive desktop functionality
        such as the windowing manager, the filesystem classes and underlying technology
        - the coolest demo right now is probably Quake 3.
        Please note that this is <i>the actual game</i>, compiled from
        C/C++ to JavaScript. It delivers 60fps [which is a limit imposed by
        the browser, not JavaScript] and runs silky smooth even on
        cheap, single board computers like the Tinkerboard or ODroid XU4.</p>

        ';

  UpdateScrollValues();
  {var LSpacing := Content.Scrollbox.Content.Border.GetVSpace();
  var LScrollHeight := Content.Scrollbox.Content.ScrollInfo.ScrollHeight;
  var LContHeight := Content.Scrollbox.Content.Height;
  inc(LScrollHeight, LSpacing);
  inc(LContHeight, LSpacing);
  Content.Scrollbox.Content.Height := max(LScrollHeight, LContHeight);   }

  RightEdge.Scrollbar.OnChanged :=  @WindowScrollbarScrolled;
  Content.Scrollbox.OnScroll := @WindowContentScrolled;
end;

procedure TWbWelcomeWindow.UpdateScrollValues;
begin
  Content.Scrollbox.Content.height := 0;
  TW3Dispatch.Execute( procedure ()
  begin
    var LFirst := TVariant.AsInteger( Content.Scrollbox.Content.Handle.offsetHeight );
    var LSecond := Content.Scrollbox.Content.ScrollInfo.ScrollHeight;
    var LBounds := Content.Scrollbox.Container.ClientRect();

    var LSize := max(LFirst, LSecond);
    if ((LSize div 2) * 2) < LSize then
      inc(LSize);

    Content.Scrollbox.Content.width := LBounds.width - 10;
    Content.Scrollbox.Content.Height := LSize;

    RightEdge.Scrollbar.Total := LSize + 1;
    RightEdge.Scrollbar.PageSize := LBounds.Height;

  end, 100);
end;

procedure TWbWelcomeWindow.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    UpdateScrollValues();
  end;
end;

procedure TWbWelcomeWindow.AfterResize;
begin
  if (csReady in ComponentState) then
    UpdateScrollValues();
end;

//#############################################################################
// TWbWelcomeContent
//#############################################################################

procedure TWbWelcomeContent.InitializeObject;
begin
  inherited;
  FBox := TWbWelcomeScrollBox.Create(self);
  FBox.ScrollBars := sbIndicator;
  FBox.ThemeReset();
end;

procedure TWbWelcomeContent.FinalizeObject;
begin
  FBox.free;
  inherited;
end;

procedure TWbWelcomeContent.Resize;
begin
  inherited;
  FBox.SetBounds(ClientRect);
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'

    .TWbWelcomeWindow {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
    }

    .TWbWelcomeScrollBox {
      padding: 0px !important;
      margin: 0px !important;
    }

    .TWbWelcomeScrollBox > * {
      padding: 0px !important;
      margin: 0px !important;
    }

    .DocHeader {
      font-weight: bold;
      word-break: keep-all;
      font-size: 28px;
      line-height: 28px;
      margin-left: 4px;
    }

    .TopicHeader {
      margin-left: 4px;
      word-break: keep-all;
      font-size: 16px;
      line-height: 16px;
      font-weight: bold;
    }


    .DocText {
      font-size: initial;
      font-weight: normal;
      margin-left: 20px;
    }

  ';
  StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;

end.
